package main

import (
	socket "logProviderAPI/App/GorillaSocket"
	"logProviderAPI/App/Routes"
)

var err error

func main() {
	go socket.InitSocket()

	r := Routes.SetupRouter()
	r.Run(":8082")

}
