package Controllers

import (
	socket "logProviderAPI/App/GorillaSocket"
	"logProviderAPI/App/Models"
	"net/http"

	"github.com/gin-gonic/gin"
)

// GetSocketUrl ... Get new socket url
func GetSocketUrl(c *gin.Context) {
	var socketInfo Models.Socket
	socket.GetSocketInfo(&socketInfo)
	c.JSON(http.StatusOK, socketInfo)
}
