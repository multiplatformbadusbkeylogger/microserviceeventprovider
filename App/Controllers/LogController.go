package Controllers

import (
	"fmt"
	"logProviderAPI/App/Models"
	"net/http"

	"github.com/gin-gonic/gin"
)

// CheckOptions ... For Flutter app connection
func CheckOptions(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
	c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
	c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")
	c.JSON(http.StatusOK, "OK")
}

// GetDevices ... Get all devices
func GetDevices(c *gin.Context) {
	var devices []Models.Device
	err := Models.GetAllDevices(&devices)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusUnprocessableEntity, Models.SetError(Models.READ_WRITE_ERROR))
	} else {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")
		c.JSON(http.StatusOK, devices)
	}
}

// GetMessagesByDevice ... Get all messages by device
func GetMessagesByDevice(c *gin.Context) {
	mac := c.Query("mac")

	if mac != "" {
		var messages []Models.Message
		err := Models.GetAllMessagesByDevice(&messages, mac)
		if err != nil {
			fmt.Println(err)
			c.JSON(http.StatusUnprocessableEntity, Models.SetError(Models.READ_WRITE_ERROR))
		} else {
			c.JSON(http.StatusOK, messages)
		}
	}
}
