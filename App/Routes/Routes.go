package Routes

import (
	"logProviderAPI/App/Controllers"

	"github.com/gin-gonic/gin"
)

// SetupRouter ... Configure routes
func SetupRouter() *gin.Engine {
	r := gin.Default()
	grp1 := r.Group("/api")
	{
		grp1.OPTIONS("log/getDevices", Controllers.CheckOptions)
		grp1.GET("log/getDevices", Controllers.GetDevices)
		grp1.OPTIONS("log/getMessagesByDevice", Controllers.CheckOptions)
		grp1.GET("log/getMessagesByDevice", Controllers.GetMessagesByDevice)
	}

	return r
}
