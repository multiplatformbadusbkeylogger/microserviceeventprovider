package GorillaSocket

import (
	"encoding/json"
	"flag"
	"log"
	"logProviderAPI/App/Models"
	"net/http"
	"text/template"
	"time"

	"github.com/gorilla/websocket"
)

var _upgrader = websocket.Upgrader{} // use default options
var _connectionsMap map[string]*websocket.Conn

func echo(w http.ResponseWriter, r *http.Request) {
	_upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	c, err := _upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer c.Close()
	for {
		mt, message, err := c.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			break
		}
		log.Printf("recv: %s", message)

		err = c.WriteMessage(mt, message)
		if err != nil {
			log.Println("write:", err)
			break
		}
	}
}

func messages(w http.ResponseWriter, r *http.Request) {
	_upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	c, err := _upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer c.Close()
	for {
		mt, message, err := c.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			break
		}
		log.Printf("recv: %s", message)

		var macObject Models.Mac
		err = json.Unmarshal(message, &macObject)
		//_connectionsMap[macObject.Mac] = c
		log.Println(_connectionsMap)

		if err != nil {
			log.Println("unmarshall:", err)
			break
		}

		socketConnectionBeginMessage := Models.Message{Message: "-- SOCKET CONNECTION BEGIN --", DateTime: time.Now().UnixMilli()}
		jsonObject, _ := json.Marshal(socketConnectionBeginMessage)
		err = c.WriteMessage(mt, []byte(jsonObject))
		if err != nil {
			log.Println("write:", err)
			break
		}

		go Models.ListenForTableChanges(c, mt, macObject.Mac)

	}
}

func home(w http.ResponseWriter, r *http.Request) {
	homeTemplate.Execute(w, "ws://"+r.Host+"/messages")
}

func GetSocketInfo(socketInfo *Models.Socket) {
	socketInfo.Port = "8083"
	socketInfo.EndPoint = "messages"
}

func InitSocket() {
	var addr = flag.String("addr", "type_address", "http service address")
	_connectionsMap = make(map[string]*websocket.Conn)
	flag.Parse()
	log.SetFlags(0)
	http.HandleFunc("/echo", echo)
	http.HandleFunc("/messages", messages)
	http.HandleFunc("/", home)
	log.Fatal(http.ListenAndServe(*addr, nil))
}

var homeTemplate = template.Must(template.New("").Parse(`
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<script>  
window.addEventListener("load", function(evt) {

    var output = document.getElementById("output");
    var input = document.getElementById("input");
    var ws;

    var print = function(message) {
        var d = document.createElement("div");
        d.textContent = message;
        output.appendChild(d);
        output.scroll(0, output.scrollHeight);
    };

    document.getElementById("open").onclick = function(evt) {
        if (ws) {
            return false;
        }
        ws = new WebSocket("{{.}}");
        ws.onopen = function(evt) {
            print("OPEN");
        }
        ws.onclose = function(evt) {
            print("CLOSE");
            ws = null;
        }
        ws.onmessage = function(evt) {
            print("RESPONSE: " + evt.data);
        }
        ws.onerror = function(evt) {
            print("ERROR: " + evt.data);
        }
        return false;
    };

    document.getElementById("send").onclick = function(evt) {
        if (!ws) {
            return false;
        }
        print("SEND: " + input.value);
        ws.send(input.value);
        return false;
    };

    document.getElementById("close").onclick = function(evt) {
        if (!ws) {
            return false;
        }
        ws.close();
        return false;
    };

});
</script>
</head>
<body>
<table>
<tr><td valign="top" width="50%">
<p>Click "Open" to create a connection to the server, 
"Send" to send a message to the server and "Close" to close the connection. 
You can change the message and send multiple times.
<p>
<form>
<button id="open">Open</button>
<button id="close">Close</button>
<p><input id="input" type="text" value="Hello world!">
<button id="send">Send</button>
</form>
</td><td valign="top" width="50%">
<div id="output" style="max-height: 70vh;overflow-y: scroll;"></div>
</td></tr></table>
</body>
</html>
`))
