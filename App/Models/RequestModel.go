package Models

type UserId struct {
	UserId uint `json:"userId"`
}

type UserMacRequest struct {
	UserId      uint   `json:"userId"`
	MAC         string `json:"mac"`
	DetectionOn bool   `json:"detectionOn"`
}
