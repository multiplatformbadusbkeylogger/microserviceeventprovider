package Models

type Number int

const (
	READ_WRITE_ERROR          Number = iota //  0
	LOGIN_ERROR                             // 1
	NO_NAME_OR_PASSWORD_GIVEN               // 2
	USER_ALREADY_EXISTS                     // …
	BAD_REQUEST                             // …
)

type ErrorResponse struct {
	ErrorNumber Number
}

func SetError(number Number) ErrorResponse {
	var errorResponse ErrorResponse
	errorResponse.ErrorNumber = number
	return errorResponse
}
