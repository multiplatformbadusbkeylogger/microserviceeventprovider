package Models

type Device struct {
	Mac          string `json:"mac"`
	Os           string `json:"os"`
	LastMessage  string `json:"lastmessage"`
	LastDateTime int64  `json:"lastdatetime"`
}

type Message struct {
	Mac      string `json:"mac" rethinkdb:"mac"`
	Message  string `json:"message" rethinkdb:"message"`
	Os       string `json:"os" rethinkdb:"os"`
	DateTime int64  `json:"datetime" rethinkdb:"datetime"`
}

type Socket struct {
	Port     string `json:"port"`
	EndPoint string `json:"endpoint"`
}

type Mac struct {
	Mac string `json:"mac"`
}

type RethinkDBUpdate struct {
	NewVal interface{} `json:"new_val"`
}
